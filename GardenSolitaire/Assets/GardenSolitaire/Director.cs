﻿using UnityEngine;
using System.Collections;

public delegate void GameEndCallback(bool victory);

public class Director : MonoBehaviour {

    public static GameEndCallback GameEndEvent;

    public static void EndGame(bool victory)
    {
        if (GameEndEvent != null) GameEndEvent(victory);
    }

}
