﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Hand : MonoBehaviour {

    public int initialCards = 7;

    void Start()
    {
        Deck.DealCardEvent += OnDealCard;
        Card.CardRemovedEvent += OnCardRemoved;
        StartCoroutine(DealHand());
    }

    void OnDestroy()
    {
        Deck.DealCardEvent -= OnDealCard;
        Card.CardRemovedEvent -= OnCardRemoved;
    }

    void OnDealCard(GameObject card)
    {
        card.SetActive(true);
        card.transform.parent = transform;
        RefreshCardPositions();
    }

    void OnCardRemoved() {
        RefreshCardPositions();
        CheckGameOver();
    }

    IEnumerator DealHand() {
        for (int i=0; i<initialCards; ++i) {
            Deck.DrawCard();
            yield return new WaitForSeconds(0.1f);
        }
        CheckGameOver(true);
    }

    void RefreshCardPositions()
    {
        float x0 = ((float)transform.childCount - 1) * -0.5f;
        for (int i=0; i<transform.childCount; ++i) {
            var child = transform.GetChild(i);
            StartCoroutine(MoveCardTo(child, new Vector3(x0+i, 0.0f, 0.0f)));
        }
    }

    IEnumerator MoveCardTo(Transform child, Vector3 dest) {
        Vector3 pos = child.localPosition;
        float elapsed = 0;
        float time = 0.2f;
        while (elapsed < time) {
            elapsed += Time.deltaTime;
            float t = elapsed/time;
            child.localPosition = Vector3.Lerp(pos, dest, t*t);
            yield return null;
        }
        child.localPosition = dest;
    }

    bool HasFlowerCard()
    {
        foreach (Transform child in transform) {
            var card = child.GetComponent<Card>();
            if (card.IsFlowerCard()) return true;
        }
        return false;
    }

    bool HasWaterCard()
    {
        foreach (Transform child in transform) {
            var card = child.GetComponent<Card>();
            if (card.IsWaterCard()) return true;
        }
        return false;
    }

    void CheckGameOver(bool quickReshuffle=false)
    {
        var board = GameObject.FindWithTag("Board").GetComponent<Board>();
        bool haveFlowerCard = HasFlowerCard();
        bool haveWaterCard = HasWaterCard();
        bool canPlaceFlower = !board.BoardFull() && haveFlowerCard;
        bool canWaterFlower = board.HasUnfinishedFlower() && haveWaterCard;
        bool gameOver = !(canPlaceFlower || canWaterFlower);
        if (gameOver) {
            if (quickReshuffle) {
                Application.LoadLevel(Application.loadedLevel);
            }
            bool victory = board.Cleared();
            Director.EndGame(victory);
        }
    }

}
