﻿using UnityEngine;
using System.Collections;

public delegate void DrawCardCallback();
public delegate void DealCardCallback(GameObject card);

[System.Serializable]
public struct DeckEntry {
    public GameObject card;
    public int quantity;
}


public class Deck : MonoBehaviour {

    public static event DrawCardCallback DrawCardEvent;
    public static event DealCardCallback DealCardEvent;

    public static void DrawCard() {
        if (DrawCardEvent != null) DrawCardEvent();
    }


    public DeckEntry[] contents;
    int cardCount;

    void Awake()
    {
        cardCount = 0;
        foreach (var entry in contents) cardCount += entry.quantity;
        DrawCardEvent += OnDrawCard;
    }

    void OnDestroy()
    {
        DrawCardEvent -= OnDrawCard;
    }

    void OnDrawCard()
    {
        if (cardCount <= 0) return;

        if (DealCardEvent != null) {
            GameObject card = InstantiateCard();
            DealCardEvent(card);
        }
    }

    GameObject InstantiateCard()
    {
        var cardPrefab = RandomlyPickCard();
        var card = Instantiate(cardPrefab, Vector3.zero, Quaternion.identity) as GameObject;
        card.SetActive(false);
        return card;
    }

    GameObject RandomlyPickCard()
    {
        float r = Random.value;
        float totalWeight = 0.0f;
        for (int i=0; i<contents.Length; ++i) {
            float weight = ((float)contents[i].quantity) / cardCount;
            totalWeight += weight;
            if (r <= totalWeight) {
                contents[i].quantity -= 1;
                cardCount -= 1;
                return contents[i].card;
            }
        }
        print("Error: BadCodeException");
        return null;
    }

}
