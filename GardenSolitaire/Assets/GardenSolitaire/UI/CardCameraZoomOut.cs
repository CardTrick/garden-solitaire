﻿using UnityEngine;
using System.Collections;

public class CardCameraZoomOut : MonoBehaviour {

    float aimForZoom = 3.0f;

    void Awake() {
        Deck.DrawCardEvent += OnCardsChanged;
        Card.CardRemovedEvent += OnCardsChanged;
    }

    void OnDestroy() {
        Card.CardRemovedEvent -= OnCardsChanged;
        Deck.DrawCardEvent -= OnCardsChanged;
    }

    void OnCardsChanged() {
        Card[] cards = FindObjectsOfType<Card>();
        if (cards.Length <= 10) {
            aimForZoom = 3.0f;
        } else {
            Camera cam = GetComponent<Camera>();
            float requiredWidth = cards.Length + 1;
            float requiredHeight = requiredWidth / cam.aspect;
            aimForZoom = requiredHeight / 2.0f;
        }
    }

    void Update() {
        Camera cam = GetComponent<Camera>();
        cam.orthographicSize = Mathf.Lerp(cam.orthographicSize, aimForZoom, 0.1f);
    }

}
