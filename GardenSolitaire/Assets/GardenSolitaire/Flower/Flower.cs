﻿using UnityEngine;
using System.Collections;

public delegate void FlowerGrowthCallback();

public class Flower : MonoBehaviour {

    public static event FlowerGrowthCallback FlowerGrowthEvent;

    public static float dip = 0.2f;

    public int petals = 5;
    public int rewardCards = 2;
    public int extraScore = 0;

    public bool bloomed {
        get { return growth >= petals; }
    }

    public int score {
        get {
            int baseScore = System.Math.Max(petals-4, 1);
            int s = baseScore + growth;
            if (bloomed) s += 5;
            s += extraScore;
            return s;
        }
    }

    int growth = 0;
    Vector3 initialPos;

    void Start() {
        initialPos = transform.position + Vector3.down * dip;
        transform.position = initialPos;
    }

    public virtual IEnumerator Grow() {
        float elapsed = 0.0f;
        float time = 0.3f;
        GetComponent<AudioSource>().Play();
        while (elapsed < time) {
            elapsed += Time.deltaTime;
            float t = elapsed/time;
            transform.localScale = new Vector3(1, t*t, 1);
            yield return null;
        }
        transform.localScale = Vector3.one;
        if (FlowerGrowthEvent != null) FlowerGrowthEvent();
    }

    public void Water(int amount) {
        growth += amount;
        if (bloomed) {
            growth = petals;
            StartCoroutine(Bloom());
        }
        StartCoroutine(UpdateView());
        if (FlowerGrowthEvent != null) FlowerGrowthEvent();
    }

    protected virtual IEnumerator Bloom() {
        for (int i=0; i<rewardCards; ++i) {
            Deck.DrawCard();
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

    IEnumerator UpdateView() {
        StartCoroutine(GrowUp());
        foreach (Transform child in transform) {
            string[] pieces = child.gameObject.name.Split('_');
            if (pieces[0] == "Petal") {
                int petalNumber = int.Parse(pieces[1]);
                bool showing = petalNumber < growth;
                if (!child.gameObject.activeSelf && showing) {
                    StartCoroutine(GrowPetal(child));
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }
    }

    IEnumerator GrowPetal(Transform petal) {
        petal.gameObject.SetActive(true);
        float elapsed = 0.0f;
        float time = 0.3f;
        while (elapsed < time) {
            elapsed += Time.deltaTime;
            float t = elapsed/time;
            petal.localScale = new Vector3(t*t, 1, t*t);
            yield return null;
        }
        petal.localScale = Vector3.one;
    }

    IEnumerator GrowUp() {
        float height = ((float)growth) / petals;
        var basePos = transform.position;
        var targetPos = initialPos + Vector3.up * (dip*height);
        float elapsed = 0.0f;
        float time = 0.75f;
        while (elapsed < time) {
            elapsed += Time.deltaTime;
            float t = elapsed/time;
            transform.position = Vector3.Lerp(basePos, targetPos, t);
            yield return null;
        }
        transform.position = targetPos;
    }

}
