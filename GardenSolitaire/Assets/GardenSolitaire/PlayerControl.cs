﻿using UnityEngine;
using System.Collections;

public delegate void PickCardCallback(Card card);
public delegate void PickTileCallback(Vector2 tile);
public delegate void ControlEnabledCallback(bool enabled);

public class PlayerControl : MonoBehaviour {

    public static event PickCardCallback PickCardEvent;
    public static event PickTileCallback PickTileEvent;
    public static event ControlEnabledCallback ControlEnabledEvent;

    static int semaphore = 0;
    public static bool isEnabled {
        get { return semaphore == 0; }
    }

    public static void Wait() {
        bool wasEnabled = isEnabled;
        semaphore += 1;
        if (wasEnabled != isEnabled) {
            if (ControlEnabledEvent != null) ControlEnabledEvent(isEnabled);
        }
    }

    public static void Signal() {
        bool wasEnabled = isEnabled;
        semaphore -= 1;
        if (wasEnabled != isEnabled) {
            if (ControlEnabledEvent != null) ControlEnabledEvent(isEnabled);
        }
    }

    static int boardLayerMask = 1 << 8;
    static int cardLayerMask = 1 << 9;
    static int flowerLayerMask = 1 << 10;

    void Update()
    {
        if (!isEnabled) return;

        if (Input.GetMouseButtonDown(0)) {
            Pick();
        }
    }

    void Pick()
    {
        var uiCamera = GameObject.FindWithTag("UICamera").GetComponent<Camera>();
        if (!PickCard(uiCamera)) {
            var mainCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
            PickTile(mainCamera);
        }
    }

    bool PickCard(Camera camera)
    {
        var mouseRay = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit, Mathf.Infinity, cardLayerMask)) {
            var card = hit.transform.GetComponent<Card>();
            if (card) {
                if (PickCardEvent != null) PickCardEvent(card);
                return true;
            }
        }
        return false;
    }

    bool PickTile(Camera camera)
    {
        var mouseRay = camera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(mouseRay, out hit, Mathf.Infinity, boardLayerMask | flowerLayerMask)) {
            var objPosition = hit.transform.position;
            var gridPos = new Vector2(objPosition.x, objPosition.z);
            if (PickTileEvent != null) PickTileEvent(gridPos);
            return true;
        }
        return false;
    }

}
