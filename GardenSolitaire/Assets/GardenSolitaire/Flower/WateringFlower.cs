﻿using UnityEngine;
using System.Collections;

public class WateringFlower : Flower {

    public GameObject rainPrefab;
    public int waterAmount = 2;

    override protected IEnumerator Bloom() {
        RainOnAdjecentFlowers();
        for (int i=0; i<rewardCards; ++i) {
            Deck.DrawCard();
            yield return new WaitForSeconds(0.1f);
        }
        yield return null;
    }

    void RainOnAdjecentFlowers() {
        var position = transform.position;
        var tilePos = new Vector2(position.x, position.z);
        var adjecents = new Vector2[] {
            tilePos + new Vector2(1, 0),
            tilePos + new Vector2(-1, 0),
            tilePos + new Vector2(0, 1),
            tilePos + new Vector2(0, -1)
        };
        var board = GameObject.FindWithTag("Board").GetComponent<Board>();
        foreach (var gridPos in adjecents) {
            var flower = board.GetFlowerAt(gridPos);
            if (flower != null && !flower.bloomed) {
                flower.Water(waterAmount);
            }
            var rainObj = Instantiate(rainPrefab, new Vector3(gridPos.x, 0, gridPos.y),
                Quaternion.identity) as GameObject;
            Destroy(rainObj, 3.0f);
        }
    }
}
