﻿using UnityEngine;
using System.Collections;

public class SpawnFlowerCard : Card {

    public GameObject spawnPrefab;

    public override bool IsFlowerCard() { return true; }

    public override bool IsPlayableAt(Vector2 tilePos) {
        var board = GameObject.FindWithTag("Board").GetComponent<Board>();
        var existingFlower = board.GetFlowerAt(tilePos);
        return existingFlower == null;
    }

    public override IEnumerator CardEffect(Vector2 tilePos) {
        int x = Mathf.RoundToInt(tilePos.x);
        int z = Mathf.RoundToInt(tilePos.y);
        var pos = new Vector3(x, 0, z);
        var rot = Quaternion.AngleAxis(Random.value*360, Vector3.up);
        var flowerObj = Instantiate(spawnPrefab, pos, rot) as GameObject;
        var flower = flowerObj.GetComponent<Flower>();
        yield return flower.StartCoroutine(flower.Grow());
    }

}
