﻿using UnityEngine;
using System.Collections;

public class WaterCard : Card {

    public GameObject rainPrefab;

    public int waterAmount = 1;

    public override bool IsWaterCard() { return true; }

    public override bool IsPlayableAt(Vector2 tilePos) {
        var board = GameObject.FindWithTag("Board").GetComponent<Board>();
        var existingFlower = board.GetFlowerAt(tilePos);
        return (existingFlower != null && !existingFlower.bloomed);
    }

    public override IEnumerator CardEffect(Vector2 tilePos) {
        var board = GameObject.FindWithTag("Board").GetComponent<Board>();
        var flower = board.GetFlowerAt(tilePos);
        var rainObj = Instantiate(rainPrefab, new Vector3(tilePos.x, 0, tilePos.y),
            Quaternion.identity) as GameObject;
        Destroy(rainObj, 3.0f);
        flower.Water(waterAmount);
        yield return new WaitForSeconds(1.0f);
    }

}
