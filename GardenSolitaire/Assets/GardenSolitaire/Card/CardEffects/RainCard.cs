﻿using UnityEngine;
using System.Collections;

public class RainCard : Card {

    public GameObject rainPrefab;

    public int waterAmount = 2;

    public override bool IsWaterCard() { return true; }

    public override bool IsPlayableAt(Vector2 tilePos) {
        return true;
    }

    public override IEnumerator CardEffect(Vector2 tilePos) {
        Flower[] flowers = FindObjectsOfType<Flower>();
        foreach (Flower flower in flowers) {
            if (flower.bloomed) continue;
            var rainObj = Instantiate(rainPrefab, flower.transform.position,
                Quaternion.identity) as GameObject;
            Destroy(rainObj, 3.0f);
            flower.Water(waterAmount);
        }
        yield return new WaitForSeconds(1.0f);
    }

}
