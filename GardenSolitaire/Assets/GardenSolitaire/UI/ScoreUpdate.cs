﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreUpdate : MonoBehaviour {

    void Awake()
    {
        Flower.FlowerGrowthEvent += OnFlowerGrowth;
    }

    void OnDestroy()
    {
        Flower.FlowerGrowthEvent -= OnFlowerGrowth;
    }

    void OnFlowerGrowth()
    {
        var board = GameObject.FindWithTag("Board").GetComponent<Board>();
        int score = board.GetScore();
        GetComponent<Text>().text = score.ToString();
    }    
}