﻿using UnityEngine;
using System.Collections;

public delegate void CardRemovedCallback();

[System.Serializable]
public struct FlowerStats {
    public bool enabled;
    public int petals;
    public int cards;
}

[System.Serializable]
public struct WaterStats {
    public bool enabled;
    public string waterAmount;
}

public abstract class Card: MonoBehaviour {

    public static event CardRemovedCallback CardRemovedEvent;

    public GameObject cardViewPrefab;
    public string cardName;
    public Material cardImage;
    public Material cardBacking;
    public FlowerStats flowerStats;
    public WaterStats waterStats;

    bool selected = false;

    public abstract bool IsPlayableAt(Vector2 tilePos);
    public abstract IEnumerator CardEffect(Vector2 tilePos);
    public virtual bool IsFlowerCard() { return false; }
    public virtual bool IsWaterCard() { return false; }


    void Awake() {
        PlayerControl.PickCardEvent += OnPickCard;
        PlayerControl.PickTileEvent += OnPickTile;
        ConstructView();
    }

    void OnDestroy() {
        PlayerControl.PickTileEvent -= OnPickTile;
        PlayerControl.PickCardEvent -= OnPickCard;
    }

    void OnPickTile(Vector2 tilePos) {
        if (selected && IsPlayableAt(tilePos)) {
            StartCoroutine(PlayCard(tilePos));
        }
    }

    void OnPickCard(Card card) {
        bool wasSelected = selected;
        selected = (card == this);
        if (selected != wasSelected) {
            StartCoroutine(Rescale());
        }
    }

    IEnumerator PlayCard(Vector2 tilePos) {
        PlayerControl.PickCardEvent -= OnPickCard;
        selected = false;
        PlayerControl.Wait();
        StartCoroutine(DropCard());
        yield return StartCoroutine(CardEffect(tilePos));
        PlayerControl.Signal();
        transform.parent = null;
        Destroy(gameObject, 3.0f);
        if (CardRemovedEvent != null) CardRemovedEvent();
    }

    IEnumerator DropCard() {
        Vector3 velocity = Vector3.zero;
        float time = 3.0f;
        while (time > 0) {
            time -= Time.deltaTime;
            velocity += Physics.gravity * Time.deltaTime;
            transform.position += velocity * Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator Rescale() {
        Vector3 scale = transform.localScale;
        Vector3 targetScale = selected ? new Vector3(1.75f, 1.75f, 1) : Vector3.one;
        Vector3 pos = transform.localPosition;
        Vector3 targetPos = selected ? new Vector3(pos.x, 0.5f, -0.5f) : new Vector3(pos.x, 0, 0);
        float elapsed = 0.0f;
        float time = 0.2f;
        while (elapsed < time) {
            elapsed += Time.deltaTime;
            float t = elapsed/time;
            transform.localScale = Vector3.Lerp(scale, targetScale, t*t);
            transform.localPosition = Vector3.Lerp(pos, targetPos, t*t);
            yield return null;
        }
        transform.localScale = targetScale;
    }

    void ConstructView() {
        var viewObj = Instantiate(cardViewPrefab) as GameObject;
        var view = viewObj.transform;
        view.parent = transform;
        view.localPosition = Vector3.zero;
        view.localRotation = Quaternion.identity;
        view.localScale = new Vector3(0.9f, 0.9f, 1.0f);
        view.GetComponent<Renderer>().material = cardBacking;
        view.Find("Picture").GetComponent<Renderer>().material = cardImage;
        view.Find("CardName").GetComponent<TextMesh>().text = cardName;
        var fstats = view.Find("FlowerStats");
        if (flowerStats.enabled) {
            fstats.Find("PetalsText").GetComponent<TextMesh>().text = flowerStats.petals.ToString();
            fstats.Find("CardsText").GetComponent<TextMesh>().text = "+"+flowerStats.cards.ToString();
        } else {
            fstats.gameObject.SetActive(false);            
        }
        var wstats = view.Find("WaterStats");
        if (waterStats.enabled) {
            wstats.Find("WaterText").GetComponent<TextMesh>().text = waterStats.waterAmount;
        } else {
            wstats.gameObject.SetActive(false);
        }
    }
}
