﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HighScoreText : MonoBehaviour {

    void Awake()
    {
        int highScore = PlayerPrefs.GetInt("HighScore", 0);
        GetComponent<Text>().text = highScore.ToString();
    }

}
