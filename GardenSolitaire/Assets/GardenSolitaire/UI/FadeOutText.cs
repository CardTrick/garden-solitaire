﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeOutText : MonoBehaviour {

    public float delay = 0.0f;
    public float time = 3.0f;

    int skipClicks = 0;

    void Awake()
    {
        var text = GetComponent<Text>();
        StartCoroutine(FadeOut(text));
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0)) {
            skipClicks += 1;
        }
        if (skipClicks == 2) {
            StopAllCoroutines();
            gameObject.SetActive(false);
        }
    }

    IEnumerator FadeOut(Text text) {
        yield return new WaitForSeconds(delay);
        float elapsed = 0.0f;
        var startColor = text.color;
        var endColor = new Color(startColor.r, startColor.g, startColor.b, 0.0f);
        while (elapsed < time) {
            elapsed += Time.deltaTime;
            float t = elapsed/time;
            text.color = Color.Lerp(startColor, endColor, t);
            yield return null;
        }
        gameObject.SetActive(false);
    }

}
