﻿using UnityEngine;
using System.Collections;

public class Board : MonoBehaviour {

    public static int flowerLayerMask = 1 << 10;

    public Flower GetFlowerAt(Vector2 gridPos) {
        var above = new Vector3(gridPos.x, 10.0f, gridPos.y);
        RaycastHit hit;
        bool didHit = Physics.SphereCast(above, 0.4f, Vector3.down,
            out hit, Mathf.Infinity, flowerLayerMask);
        if (didHit) {
            var flower = hit.transform.parent.GetComponent<Flower>();
            return flower;
        }
        return null;
    }

    public int GetScore() {
        int score = 0;
        for (int y=-1; y<=1; ++y) {
            for (int x=-1; x<=1; ++x) {
                var tilePos = new Vector2(x, y);
                var flower = GetFlowerAt(tilePos);
                if (flower != null) {
                    score += flower.score;
                }
            }
        }
        if (Cleared()) {
            score += 20;
        }
        return score;
    }

    public bool BoardFull() {
        for (int y=-1; y<=1; ++y) {
            for (int x=-1; x<=1; ++x) {
                var tilePos = new Vector2(x, y);
                var flower = GetFlowerAt(tilePos);
                if (flower == null) {
                    return false;
                }
            }
        }
        return true;
    }

    public bool HasUnfinishedFlower() {
        for (int y=-1; y<=1; ++y) {
            for (int x=-1; x<=1; ++x) {
                var tilePos = new Vector2(x, y);
                var flower = GetFlowerAt(tilePos);
                if (flower != null && !flower.bloomed) {
                    return true;
                }
            }
        }
        return false;
    }

    public bool Cleared() {
        for (int y=-1; y<=1; ++y) {
            for (int x=-1; x<=1; ++x) {
                var tilePos = new Vector2(x, y);
                var flower = GetFlowerAt(tilePos);
                if (flower == null) {
                    return false;
                } else {
                    if (!flower.bloomed) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

}
