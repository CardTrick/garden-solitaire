﻿using UnityEngine;
using System.Collections;

public class UniquePersist : MonoBehaviour {

    bool isTheOne = false;

    void Awake() {
        var objs = GameObject.FindGameObjectsWithTag(gameObject.tag);
        if (objs.Length > 1) {
            Destroy(gameObject);
        } else {
            DontDestroyOnLoad(gameObject);
            isTheOne = true;
        }
    }

}
