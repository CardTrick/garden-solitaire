﻿using UnityEngine;
using System.Collections;

public class GameEndPanel : MonoBehaviour {

    void Awake()
    {
        Director.GameEndEvent += OnGameEnd;
    }

    void OnDestroy()
    {
        Director.GameEndEvent -= OnGameEnd;
    }

    void OnGameEnd(bool victory)
    {
        var panel = transform.Find("Panel");
        panel.gameObject.SetActive(true);
        if (victory) {
            panel.Find("GameOverText").gameObject.SetActive(false);
        } else {
            panel.Find("ClearText").gameObject.SetActive(false);
        }
        SaveHighScore();
    }

    public void Replay() 
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    void SaveHighScore() {
        var board = GameObject.FindWithTag("Board").GetComponent<Board>();
        int score = board.GetScore();
        int highScore = PlayerPrefs.GetInt("HighScore", 0);
        int newHighScore = System.Math.Max(highScore, score);
        PlayerPrefs.SetInt("HighScore", newHighScore);
    }

}
